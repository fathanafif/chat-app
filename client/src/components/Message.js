import React from 'react';
import ReactEmoji from 'react-emoji';
import './Chat.css';

const Message = ({message: {user, text}, name}) => {
    let isSentByCurrentUser = false;
    let test = "test";

    if(user === name) {
        isSentByCurrentUser = true;
    }
    
    return (
        isSentByCurrentUser
        ? (
            <div className="message-container justify-end text-right">
                <div className="message-box">
                    <p className="messageText colorDark">{ReactEmoji.emojify(text)}</p>
                </div>
                <p className="sent-text pl-10 ">{user}</p>
            </div>
            )
            : (
                <div className="message-container justify-start">
                    <div className="message-box">
                        <p className="messageText colorDark">{ReactEmoji.emojify(text)}</p>
                    </div>
                    <p className="sent-text pl-10">{user}</p>
                </div>
            )
    );
                
};

export default Message;