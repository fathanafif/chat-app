import React from 'react';
import ScrollToBottom from 'react-scroll-to-bottom';
import Message from './Message';

const Messages = ({messages, name}) => (
    <div className="card-body">
        <ScrollToBottom className="col-12 p-0">
            {messages.map((message, i) => <div key={i}><Message message={message} name={name}/></div>)}
        </ScrollToBottom>
    </div>
    )
    
    export default Messages;