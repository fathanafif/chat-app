import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Join.css';

const Join = () => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');

    return(
        <div className="join-container col col-12 px-0">
            <div className="login-form col col-4 py-5 px-5">
                <form action="">
                    <input placeholder="Name" className="form-control mb-3 login" type="text" onChange={(event) => setName(event.target.value)}/>
                    <input placeholder="Room" className="form-control my-3 login" type="text" onChange={(event) => setRoom(event.target.value)}/>
                    <div className="mt-3">
                        <Link onClick={e => (!name || !room) ? e.preventDefault() : null} to={`/chat?name=${name}&room=${room}`}>
                            <button className="btn btn-primary col-12" type="submit">Sign In</button>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Join;