import React from 'react';

const Input = ({message, setMessage, sendMessage}) => (
    <div className="card-footer">
        <form>
            <div className="input-group">
                <input
                    className="form-control message-input"
                    type="text"
                    placeholder="Message.."
                    value={message}
                    onChange={(event) => setMessage(event.target.value)}
                    onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
                />
                <div className="input-group-append">
                    <span className="btn input-group-text send-append" onClick={e => sendMessage(e)}>
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" className="bi bi-cursor-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103z"/>
                        </svg>
                    </span>
                </div>
            </div>
        </form>
    </div>
    )
    
    export default Input;