import React from 'react';
import {
    Menu,
    MenuItem,
    MenuButton
} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';

const InfoBar = ({room}) => (
        <div className="card-header">
            <div className="row">
                <div className="col-10">
                    {room}
                </div>
                <div className="col-2 text-right">                   
                    <Menu menuButton={
                        <MenuButton className="three-dots">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-three-dots-vertical" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                            </svg>
                        </MenuButton>
                    }>
                        <MenuItem className="menu-item" href={`./`}>Logout</MenuItem>
                    </Menu>
                </div>
            </div>
            {/* <img className="onlineIcon" src={onlineIcon} alt=""/> */}
        </div>
    )

export default InfoBar;